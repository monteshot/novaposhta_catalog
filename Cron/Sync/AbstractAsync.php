<?php

namespace Perspective\NovaposhtaCatalog\Cron\Sync;

use Magento\Framework\Serialize\SerializerInterface;

abstract class AbstractAsync
{
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected SerializerInterface $serialize;

    public function __construct(
        SerializerInterface $serialize
    ) {
        $this->serialize = $serialize;
    }

    /**
     * @return string
     */
    abstract public function execute();
}
