<?php

namespace Perspective\NovaposhtaCatalog\Cron;

use Perspective\NovaposhtaCatalog\Cron\Sync\SyncAll;
use Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate;

class Update
{
    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate
     */
    private $cronSyncDateLastUpdate;
    /**
     * @var \Perspective\NovaposhtaCatalog\Controller\Adminhtml\Sync\SyncAll
     */
    private $syncAll;

    /**
     * Update constructor.
     * @param \Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate $cronSyncDateLastUpdate
     * @param \Perspective\NovaposhtaCatalog\Controller\Adminhtml\Sync\SyncAll $syncAll
     */
    public function __construct(
        CronSyncDateLastUpdate $cronSyncDateLastUpdate,
        SyncAll $syncAll
    ) {
        $this->cronSyncDateLastUpdate = $cronSyncDateLastUpdate;
        $this->syncAll = $syncAll;
    }

    public function execute()
    {
        $this->syncAll->execute();
    }
}
