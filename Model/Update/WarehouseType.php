<?php


namespace Perspective\NovaposhtaCatalog\Model\Update;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Perspective\NovaposhtaCatalog\Helper\Config;
use Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes\Collection;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes\CollectionFactory;
use Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseTypesFactory;

/**
 * Class WarehouseType
 * Sync Types of novaposhta warehouse and sets to db (Admin and cron)
 */
class WarehouseType
{
    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\Config
     */
    protected $configHelper;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseFactory
     */
    protected $warehouseTypesFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse
     */
    protected $warehouseTypesResourceModel;

    /**
     * @var string
     */
    protected $lang = 'ua';

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes\Collection
     */
    protected $warehouseTypesResourceModelCollection;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes\CollectionFactory
     */
    protected $warehouseTypesResourceModelCollectionFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;

    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate
     */
    private $cronSyncDateLastUpdate;


    /**
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Perspective\NovaposhtaCatalog\Helper\Config $configHelper
     * @param \Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate $cronSyncDateLastUpdate
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseTypesFactory $warehouseTypesFactory
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes $warehouseTypesResourceModel
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes\Collection $warehouseTypesResourceModelCollection
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\WarehouseTypes\CollectionFactory $warehouseTypesResourceModelCollectionFactory
     */
    public function __construct(
        ZendClientFactory $httpClientFactory,
        Config $configHelper,
        CronSyncDateLastUpdate $cronSyncDateLastUpdate,
        Json $jsonSerializer,
        WarehouseTypesFactory $warehouseTypesFactory,
        WarehouseTypes $warehouseTypesResourceModel,
        Collection $warehouseTypesResourceModelCollection,
        CollectionFactory $warehouseTypesResourceModelCollectionFactory
    ) {
        $this->warehouseTypesResourceModelCollectionFactory = $warehouseTypesResourceModelCollectionFactory;
        $this->warehouseTypesResourceModelCollection = $warehouseTypesResourceModelCollection;
        $this->warehouseTypesResourceModel = $warehouseTypesResourceModel;
        $this->warehouseTypesFactory = $warehouseTypesFactory;
        $this->httpClientFactory = $httpClientFactory;
        $this->configHelper = $configHelper;
        $this->jsonSerializer = $jsonSerializer;
        $this->cronSyncDateLastUpdate = $cronSyncDateLastUpdate;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $message = "Error has been occur";
        $error = true;
        $data = [];
        if ($this->configHelper->getIsEnabledConfig()) {
            $langArr = ['ua', 'ru'];
            foreach ($langArr as $idx => $lang) {
                $this->lang = $lang;
                $warehouseTypesListJsonEncoded = $this->getWarehouseTypesListFromApiEndpoint($this->lang);
                $warehouseTypeListJsonDecoded = json_decode($warehouseTypesListJsonEncoded);
                if (property_exists($warehouseTypeListJsonDecoded, 'success')
                    && $warehouseTypeListJsonDecoded->success === true) {
                    try {
                        $error = false;
                        $message = 'In Progress..';
                        $this->setWareHouseDataToDB($warehouseTypeListJsonDecoded->data);
                    } catch (AlreadyExistsException $e) {
                        $error = true;
                        $message = "Key already exist\n" . $e->getMessage();
                    }
                    if (!$error) {
                        $error = false;
                        $message = "Successfully synced";
                        $this->cronSyncDateLastUpdate
                            ->updateSyncDate($this->cronSyncDateLastUpdate::XML_PATH_LAST_SYNC_WAREHOUSE_TYPES);
                    }
                }
            }
        }
        return [
            'message' => $message,
            'data' => $data,
            'error' => $error
        ];
    }

    /**
     * @param array $data
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setWareHouseDataToDB(array $data)
    {
        $entireTableColl = $this->warehouseTypesResourceModelCollectionFactory->create();
        $entireIds = $entireTableColl->getAllIds();
        foreach ($data as $idx => $datum) {
            $filledModel = $this->prepareData($datum);
            /**@var $warehouseTypesModel \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseTypes */
            $singleItem = $this->warehouseTypesFactory->create();
            $this->warehouseTypesResourceModel->load($singleItem, $filledModel->getRef(), $filledModel::REF);
            switch ($this->lang) {
                case "ru":
                    if ($singleItem->getRef()) {
                        $filledModel->setId($singleItem->getId());
                        $this->warehouseTypesResourceModel
                            ->save($filledModel);
                    } else {
                        $this->warehouseTypesResourceModel
                            ->save($filledModel);
                    }
                    break;
                case "ua":
                    if ($singleItem->getRef()) {
                        $filledModel->setId($singleItem->getId());
                        $this->warehouseTypesResourceModel
                            ->save($filledModel);
                    } else {
                        $this->warehouseTypesResourceModel
                            ->save($filledModel);
                    }
                    break;
            }
            unset($entireIds[array_search($singleItem->getId(), $entireIds)]);
        }
        if (count($entireIds) > 0) {
            foreach ($entireIds as $remIdx => $remItem) {
                $cleanUpModel = $this->warehouseTypesFactory->create();
                $this->warehouseTypesResourceModel->load($cleanUpModel, $remItem, 'id');
                $this->warehouseTypesResourceModel->delete($cleanUpModel);
            }
        }
    }

    /**
     * @param $datum
     * @return \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseTypes
     */
    public function prepareData($datum)
    {
        /**@var $warehouseTypesModel \Perspective\NovaposhtaCatalog\Model\Warehouse\WarehouseTypes */
        $warehouseTypesModel = $this->warehouseTypesFactory->create();
        if ($this->lang === 'ua') {
            isset($datum->Description) ? $warehouseTypesModel->setDescriptionUa($datum->Description) : null;
        } else {
            isset($datum->Description) ? $warehouseTypesModel->setDescriptionRu($datum->Description) : null;
        }
        isset($datum->Ref) ? $warehouseTypesModel->setRef($datum->Ref) : null;
        return $warehouseTypesModel;
    }

    /**
     * @param $lang
     * @return mixed
     */
    protected function getWarehouseTypesListFromApiEndpoint($lang)
    {
        $apiKey = $this->configHelper->getApiKeyConfig();
        $request = $this->httpClientFactory->create();
        $request->setUri('https://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses');
        $params = ['modelName' => 'AddressGeneral', 'calledMethod' => 'getWarehouseTypes', 'apiKey' => $apiKey,
            'methodProperties' => ['Language' => $lang]];
        $request->setConfig(['maxredirects' => 0, 'timeout' => 60]);
        $request->setRawData(utf8_encode($this->jsonSerializer->serialize($params)));
//        $request->setRawData(utf8_encode(json_encode($params)));
        return $request->request(\Zend_Http_Client::POST)->getBody();
    }
}
