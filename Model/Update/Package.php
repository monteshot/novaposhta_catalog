<?php


namespace Perspective\NovaposhtaCatalog\Model\Update;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Perspective\NovaposhtaCatalog\Helper\Config;
use Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate;
use Perspective\NovaposhtaCatalog\Model\Package\PackageFactory;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Package\Package as PackageResourceModel;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Package\Package\CollectionFactory;
use stdClassFactory;

/**
 * Class Package
 * Sync Types of novaposhta Packages and sets to db (Admin and cron)
 */
class Package
{
    /**
     * @var \Perspective\NovaposhtaCatalog\Model\Package\PackageFactory
     */
    public $packageFactory;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\Config
     */
    protected $configHelper;

    /**
     * @var string
     */
    protected $lang = 'ua';

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;

    /**
     * @var \Perspective\NovaposhtaCatalog\Helper\CronSyncDateLastUpdate
     */
    private $cronSyncDateLastUpdate;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @var \stdClass
     */
    private $jsonParams;

    /**
     * @var \stdClassFactory
     */
    private $stdClassFactory;

    private $Length;

    private $Width;

    private $Height;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Package\Package\CollectionFactory
     */
    private $packageTypesResourceModelCollectionFactory;

    /**
     * @var \Perspective\NovaposhtaCatalog\Model\ResourceModel\Package\Package
     */
    private $packageResourceModel;

    /**
     * Package constructor.
     * @param Context $context
     * @param ZendClientFactory $httpClientFactory
     * @param \stdClassFactory $stdClassFactory
     * @param Config $configHelper
     * @param JsonFactory $resultJsonFactory
     * @param CronSyncDateLastUpdate $cronSyncDateLastUpdate
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param Json $jsonSerializer
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Package\Package\CollectionFactory $packageTypesResourceModelCollectionFactory
     * @param \Perspective\NovaposhtaCatalog\Model\ResourceModel\Package\Package $packageResourceModel
     * @param \Perspective\NovaposhtaCatalog\Model\Package\PackageFactory $packageFactory
     * @param null $Length
     * @param null $Width
     * @param null $Height
     */
    public function __construct(
        ZendClientFactory $httpClientFactory,
        stdClassFactory $stdClassFactory,
        Config $configHelper,
        CronSyncDateLastUpdate $cronSyncDateLastUpdate,
        Curl $curl,
        Json $jsonSerializer,
        CollectionFactory $packageTypesResourceModelCollectionFactory,
        PackageResourceModel $packageResourceModel,
        PackageFactory $packageFactory,
        //надо понять для чего сделаны как параметры
        $Length = null,
        $Width = null,
        $Height = null
    ) {
        $this->httpClientFactory = $httpClientFactory;
        $this->configHelper = $configHelper;
        $this->jsonSerializer = $jsonSerializer;
        $this->cronSyncDateLastUpdate = $cronSyncDateLastUpdate;
        $this->curl = $curl;
        $this->Length = $Length;
        $this->Width = $Width;
        $this->Height = $Height;
        $this->stdClassFactory = $stdClassFactory;
        $this->packageTypesResourceModelCollectionFactory = $packageTypesResourceModelCollectionFactory;
        $this->packageFactory = $packageFactory;
        $this->packageResourceModel = $packageResourceModel;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $message = "Error has been occur";
        $error = true;
        $data = [];
        if ($this->configHelper->getIsEnabledConfig()) {
            $packageTypesListJsonEncoded = $this->getPackageTypesListFromApiEndpoint();
            $packageTypeListJsonDecoded = json_decode($packageTypesListJsonEncoded);
            if (property_exists($packageTypeListJsonDecoded, 'success')
                && $packageTypeListJsonDecoded->success === true) {
                try {
                    $error = false;
                    $message = 'In Progress..';
                    $this->setPackageDataToDB($packageTypeListJsonDecoded->data);
                } catch (AlreadyExistsException $e) {
                    $error = true;
                    $message = "Key already exist\n" . $e->getMessage();
                }
                if (!$error) {
                    $error = false;
                    $message = "Successfully synced";
                    $this->cronSyncDateLastUpdate
                        ->updateSyncDate($this->cronSyncDateLastUpdate::XML_PATH_LAST_SYNC_PACKAGE_TYPES);
                }
            }
        }
        return [
            'message' => $message,
            'data' => $data,
            'error' => $error
        ];
    }

    /**
     * @param $lang
     * @return mixed
     */
    protected function getPackageTypesListFromApiEndpoint()
    {
        $this->jsonParams = $this->stdClassFactory->create();
        $this->jsonParams->modelName = "Common";
        $this->jsonParams->calledMethod = "getPackList";
        $uri = 'https://api.novaposhta.ua/v2.0/json/common/getPackList';
        $methodProperties = $this->stdClassFactory->create();
        $methodProperties->Length = $this->Length ?: null;
        $methodProperties->Width = $this->Width ?: null;
        $methodProperties->Height = $this->Height ?: null;
        $this->jsonParams->methodProperties = $methodProperties;
        $apiKey = $this->configHelper->getApiKeyConfig();
        $this->curl->setOption(CURLOPT_URL, $uri);
        $this->curl->setOption(CURLOPT_POSTFIELDS, json_encode($this->jsonParams));
        $this->curl->setOption(CURLOPT_RETURNTRANSFER, 1);
        $this->curl->setOption(CURLOPT_CONNECTTIMEOUT, 300);
        $this->curl->post($uri, []);
        return $this->curl->getBody();
    }

    /**
     * @param array $data
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setPackageDataToDB($data)
    {
        $entireTableColl = $this->packageTypesResourceModelCollectionFactory->create();
        $entireIds = $entireTableColl->getAllIds();
        foreach ($data as $idx => $datum) {
            $filledModel = $this->prepareData($datum);
            /**@var $singleItem \Perspective\NovaposhtaCatalog\Model\Package\Package */
            $singleItem = $this->packageFactory->create();
            $this->packageResourceModel->load($singleItem, $filledModel->getRef(), $filledModel::REF);
            if ($singleItem->getRef()) {
                $filledModel->setId($singleItem->getId());
                $this->packageResourceModel
                    ->save($filledModel);
            } else {
                $this->packageResourceModel
                    ->save($filledModel);
            }

            unset($entireIds[array_search($singleItem->getId(), $entireIds)]);
        }
        if (count($entireIds) > 0) {
            foreach ($entireIds as $remIdx => $remItem) {
                $cleanUpModel = $this->packageFactory->create();
                $this->packageResourceModel->load($cleanUpModel, $remItem, 'id');
                $this->packageResourceModel->delete($cleanUpModel);
            }
        }
    }

    /**
     * @param $datum
     * @return \Perspective\NovaposhtaCatalog\Model\Package\Package
     */
    public function prepareData($datum)
    {
        /**@var $packageTypesModel \Perspective\NovaposhtaCatalog\Model\Package\Package */
        $packageTypesModel = $this->packageFactory->create();
        isset($datum->Description) ? $packageTypesModel->setDescriptionUa($datum->Description) : null;
        isset($datum->DescriptionRu) ? $packageTypesModel->setDescriptionRu($datum->DescriptionRu) : null;
        isset($datum->Ref) ? $packageTypesModel->setRef($datum->Ref) : null;
        isset($datum->Length) ? $packageTypesModel->setLength($datum->Length) : null;
        isset($datum->Width) ? $packageTypesModel->setWidth($datum->Width) : null;
        isset($datum->Height) ? $packageTypesModel->setHeight($datum->Height) : null;
        return $packageTypesModel;
    }
}
