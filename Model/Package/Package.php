<?php


namespace Perspective\NovaposhtaCatalog\Model\Package;

use Perspective\NovaposhtaCatalog\Api\Data\PackageInterface;

class Package extends \Magento\Framework\Model\AbstractExtensibleModel implements PackageInterface
{

    protected function _construct()
    {
        $this->_init(\Perspective\NovaposhtaCatalog\Model\ResourceModel\Package\Package::class);
    }

    public function setDescriptionUa($data)
    {
        return $this->setData(self::DESCRIPTION_UA, $data);
    }

    public function setDescriptionRu($data)
    {
        return $this->setData(self::DESCRIPTION_RU, $data);
    }

    public function setRef($data)
    {
        return $this->setData(self::REF, $data);
    }

    public function getDescriptionUa()
    {
        return $this->getData(self::DESCRIPTION_UA);
    }

    public function getDescriptionRu()
    {
        return $this->getData(self::DESCRIPTION_RU);
    }

    public function getRef()
    {
        return $this->getData(self::REF);
    }

    public function setLength($data)
    {
        return $this->setData(self::LENGTH, $data);
    }

    public function setWidth($data)
    {
        return $this->setData(self::WIDTH, $data);
    }

    public function setHeight($data)
    {
        return $this->setData(self::HEIGHT, $data);
    }

    public function getLength($data)
    {
        return $this->getData(self::LENGTH);
    }

    public function getWidth($data)
    {
        return $this->getData(self::WIDTH);
    }

    public function getHeight($data)
    {
        return $this->getData(self::HEIGHT);
    }
}
