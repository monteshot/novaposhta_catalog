<?php
declare(strict_types=1);

namespace Perspective\NovaposhtaCatalog\Model\Street;

use Magento\Framework\Model\AbstractModel;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Street\Street as StreetResource;
use Perspective\NovaposhtaCatalog\Api\Data\StreetInterface;

class Street extends AbstractModel implements StreetInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'novaposhta_street_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(StreetResource::class);
    }

    /**
     * @inheritDoc
     */
    public function getStreetId(): int
    {
        return (int)$this->getData(self::STREET_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStreetId(int $streetId): StreetInterface
    {
        return $this->setData(self::STREET_ID, $streetId);
    }

    /**
     * @inheritDoc
     */
    public function getRef(): ?string
    {
        return $this->getData(self::REF);
    }

    /**
     * @inheritDoc
     */
    public function setRef(string $ref): StreetInterface
    {
        return $this->setData(self::REF, $ref);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setDescription(string $description): StreetInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function getStreetTypeRef(): string
    {
        return $this->getData(self::STREET_TYPE_REF);
    }

    /**
     * @inheritDoc
     */
    public function setStreetTypeRef(string $streetTypeRef): StreetInterface
    {
        return $this->setData(self::STREET_TYPE_REF, $streetTypeRef);
    }

    /**
     * @inheritDoc
     */
    public function getStreetType(): string
    {
        return $this->getData(self::STREET_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setStreetType(string $streetType): StreetInterface
    {
        return $this->setData(self::STREET_TYPE, $streetType);
    }

    /**
     * @inheritDoc
     */
    public function getCityRef(): string
    {
        return $this->getData(self::CITY_REF);
    }

    /**
     * @inheritDoc
     */
    public function setCityRef(string $cityRef): StreetInterface
    {
        return $this->setData(self::CITY_REF, $cityRef);
    }
}
