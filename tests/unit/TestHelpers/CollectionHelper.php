<?php


namespace Perspective\NovaposhtaCatalog\tests\unit\TestHelpers;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class CollectionHelper
 * Create almost real collection
 */
class CollectionHelper extends \PHPUnit\Framework\TestCase
{
    /**
     * @var
     */
    public $eventManagerMock;
    /**
     * @var
     */
    public $connectionMock;
    /**
     * @var
     */
    public $selectMock;
    /**
     * @var
     */
    public $collectionMock;
    /**
     * @var
     */
    public $resourceMock;
    /**
     * @var
     */
    public $entitySnapshotMock;
    /**
     * @var
     */
    public $fetchStrategyMock;
    /**
     * @var
     */
    public $entityFactoryMock;
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Status\History\Collection
     */
    public $collection;
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objectManager;

    /**
     * @param $collectionClassName
     * @return object
     */
    public function prepareUserCollection($collectionClassName)
    {
        $this->objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->eventManagerMock = $this->createMock(\Magento\Framework\Event\ManagerInterface::class);
        $this->connectionMock = $this->createMock(\Magento\Framework\DB\Adapter\Pdo\Mysql::class);
        $this->selectMock = $this->createMock(\Magento\Framework\DB\Select::class);
        $this->collectionMock = $this->createPartialMock(
            $collectionClassName,
            ['__wakeup', 'addData']
        );
        $this->resourceMock = $this->getMockForAbstractClass(

            \Magento\Sales\Model\ResourceModel\EntityAbstract::class,
            [],
            '',
            false,
            true,
            true,
            ['getConnection', 'getMainTable', 'getTable', '__wakeup', 'getSelect']
        );
        $this->entitySnapshotMock = $this->createMock(
            \Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot::class
        );
        $this->fetchStrategyMock = $this->getMockForAbstractClass(
            \Magento\Framework\Data\Collection\Db\FetchStrategyInterface::class
        );
        $this->entityFactoryMock = $this
            ->createMock(\Magento\Framework\Data\Collection\EntityFactory::class);

        $this->resourceMock->expects($this->any())->method('getConnection')->will(
            $this->returnValue($this->connectionMock)
        );
        $this->resourceMock->method('getSelect')->will(
            $this->returnValue($this->connectionMock)
        );
        $this->resourceMock->expects($this->any())->method('getTable')->will($this->returnArgument(0));

        $this->connectionMock
            ->expects($this->any())->method('quoteIdentifier')->will($this->returnArgument(0));
        $this->connectionMock->expects($this->atLeastOnce())
            ->method('select')
            ->will($this->returnValue($this->selectMock));

        $data = [['data']];
        $this->collectionMock->expects($this->once())
            ->method('addData')
            ->with($this->equalTo($data[0]))
            ->will($this->returnValue($this->collectionMock));

        $this->fetchStrategyMock->expects($this->once())
            ->method('fetchAll')
            ->will($this->returnValue($data));

        $this->entityFactoryMock->expects($this->once())
            ->method('create')
            ->will($this->returnValue($this->collectionMock));

        $logger = $this->createMock(\Psr\Log\LoggerInterface::class);
        $configDb =
            [
                'config' => [
                    'dbname' => 'rdm',
                    'username' => 'monteshot',
                    'password' => 'monteshot'
                ]
            ];
        return $this->objectManager->getObject(
            $collectionClassName,
            [
                'entityFactory' => $this->entityFactoryMock,
                'logger' => $logger,
                'fetchStrategy' => $this->fetchStrategyMock,
                'eventManager' => $this->eventManagerMock,
                'entitySnapshot' => $this->entitySnapshotMock,
                'connection' => $this->connectionMock,
                'resource' => $this->resourceMock,
            ]
        );
    }
}
