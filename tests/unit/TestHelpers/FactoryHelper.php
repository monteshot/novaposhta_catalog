<?php


namespace Perspective\NovaposhtaCatalog\tests\unit\TestHelpers;

/**
 * Class FactoryHelper
 * Creates object with given class name
 */
class FactoryHelper extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;

    /**
     * @param $instanceName
     * @param array $param
     * @return \PHPUnit_Framework_MockObject_MockObject $instanceName
     */
    public function getMockupFactory($instanceName, $param = [])
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        /** Magento\Framework\TestFramework\Unit\Helper\ObjectManager */
        $objectManager = $this->objMan;
        $factory = $this->getMockBuilder($instanceName . 'Factory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $factory->expects($this->any())
            ->method('create')
            ->will(
                $this->returnValue($objectManager->getObject($instanceName, $param))
            );
        return $factory;
    }
}
