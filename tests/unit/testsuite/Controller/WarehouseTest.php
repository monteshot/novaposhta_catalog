<?php


namespace Perspective\NovaposhtaCatalog\tests\unit\testsuite\Controller;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\Serialize\Serializer\Json;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse as WarehouseResourceModel;
use Perspective\NovaposhtaCatalog\Model\Warehouse\Warehouse;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\Collection;
use Perspective\NovaposhtaCatalog\Model\ResourceModel\Warehouse\Warehouse\CollectionFactory;
use Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\ApiHelper;
use Perspective\NovaposhtaCatalog\Controller\Adminhtml\Sync\Warehouse as TestClass;
use Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryMockHelper;

/**
 * Class WarehouseTest
 * Test for warehouse sync
 */
class WarehouseTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;

    /**
     * @var
     */
    public $zendClientFactory;

    /**
     * @var
     */
    public $collectionFactory;

    /**
     * @var
     */
    public $cityFactory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $configHelperMock;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryHelper
     */
    public $factoryHelper;
    /**
     * @var object
     */
    public $jsonSerializer;
    /**
     * @var object
     */
    public $abstractCollection;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\CollectionHelper
     */
    public $collectionHelper;
    /**
     * @var void
     */
    public $warehouseCollectionFactory;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $connectionMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $selectMock;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryMockHelper
     */
    public $factoryMockHelper;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resultJsonFactory;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $warehouseFactory;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $warehouseResourseModel;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resultJsonMock;
    /**
     * @var \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;

    /**
     *
     */
    public function setUp()
    {
        $this->factoryHelper = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryHelper();
        $this->factoryMockHelper = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\FactoryMockHelper();
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->realObjMan = new \Perspective\NovaposhtaCatalog\tests\unit\TestHelpers\MageObjMan();
        $this->configHelperMock = $this->getMockBuilder(\Perspective\NovaposhtaCatalog\Helper\Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->jsonSerializer = $this->objMan->getObject(Json::class);
        $this->configHelperMock->method('getIsEnabledConfig')->willReturn(1);
        $this->configHelperMock->method('getApiKeyConfig')->willReturn(ApiHelper::API_KEY);
        $this->resultJsonMock = $this->getMockBuilder(\Magento\Framework\Controller\Result\Json::class)
            ->disableOriginalConstructor()->getMock();
        $this->resultJsonMock->method('setData')->willReturn($this);
        $this->resultJsonFactory = $this->getMockBuilder(JsonFactory::class)->disableOriginalConstructor()
            ->getMock();
        $this->resultJsonFactory->method('create')
            ->willReturn($this->resultJsonMock);
        $reflectionMockFactory = $this->getMockBuilder(\ReflectionClassFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->warehouseFactory = $this->factoryHelper->getMockupFactory(Warehouse::class, [
            'reflectionClassFactory' => $reflectionMockFactory
        ]);
        $this->warehouseResourseModel = $this->getMockBuilder(WarehouseResourceModel::class)
            ->disableOriginalConstructor()->getMock();
        $this->warehouseCollectionFactory = $this->getMockBuilder(CollectionFactory::class)
            ->setMethods(['create'])->disableOriginalConstructor()->getMock();
        $dbCollection = $this->realObjMan->objectManager->create(Collection::class);
        $this->warehouseCollectionFactory->method('create')->willReturn($dbCollection);
        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'context' => $this->createMock(\Magento\Backend\App\Action\Context::class),
                'httpClientFactory' => $this->factoryHelper->getMockupFactory(ZendClient::class),
                'configHelper' => $this->configHelperMock,
                'resultJsonFactory' => $this->resultJsonFactory,
                'warehouseFactory' => $this->warehouseFactory,
                'warehouseResourceModel' => $this->warehouseResourseModel,
                'warehouseResourceModelCollectionFactory' => $this->warehouseCollectionFactory,
                'jsonSerializer' => $this->jsonSerializer,

            ]
        );
    }

    /**
     * @throws \Magento\Framework\Exception\NotFoundException
     * @throws \Zend_Http_Client_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function testExecute()
    {
        $res = $this->testClass->execute();
        $jsonObj = $res->testClass->error;
        $this->assertEquals($jsonObj, false);
    }
}
